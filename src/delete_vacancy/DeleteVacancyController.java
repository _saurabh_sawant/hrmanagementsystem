package delete_vacancy;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class DeleteVacancyController {
	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button delete;
	
	@FXML
	private TextField vacancyname;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void delete(ActionEvent event) {
		System.out.println(vacancyname.getText());

		String query = " delete from addvacancy where vacancyname ='" + vacancyname.getText() + "';";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
	}
}
