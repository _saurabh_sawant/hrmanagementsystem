package vacancy;

import add_vacancy.AddVacancy;
import admin.Admin;
import delete_vacancy.DeleteVacancy;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;
import recruitment.Recruitment;
import search_vacancy.SearchVacancy;

public class VacancyController {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button addvacancy;
	
	@FXML
	private Button searchvacancy;
	
	@FXML
	private Button deletevacancy;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void exit(ActionEvent event) {
		new LoginScreen().show();
	}
	
	public void addvacancy(ActionEvent event) {
		new AddVacancy().show();
	}

	public void searchvacancy(ActionEvent event) {
		new SearchVacancy().show();
	}

	public void deletevacancy(ActionEvent event) {
		new DeleteVacancy().show();
	}
}
