package candidate;

import add_candidate.AddCandidate;
import admin.Admin;
import delete_candidate.DeleteCandidate;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;
import recruitment.Recruitment;
import search_candidate.SearchCandidate;

public class CandidateController {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;
	
	@FXML
	private Button addcandidate;
	
	@FXML
	private Button searchcandidate;
	
	@FXML
	private Button deletecandidate;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void exit(ActionEvent event) {
		new LoginScreen().show();
	}
	
	public void addcandidate(ActionEvent event) {
		new AddCandidate().show();
	}

	public void searchcandidate(ActionEvent event) {
		new SearchCandidate().show();
	}

	public void deletecandidate(ActionEvent event) {
		new DeleteCandidate().show();
	}

}
