package search_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment.Recruitment;

public class SearchVacancyController implements Initializable{
	@FXML
	private TextField vacancyname;
	
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	@FXML
	private TableView<UserVacancy> tableView;
	
	@FXML
	private TableColumn<UserVacancy, String> col1;
	@FXML
	private TableColumn<UserVacancy, String> col2;
	@FXML
	private TableColumn<UserVacancy, String> col3;
	@FXML
	private TableColumn<UserVacancy, String> col4;
	@FXML
	private TableColumn<UserVacancy, String> col5;
	
	private ObservableList<UserVacancy> data;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<UserVacancy, String>("vacancyName"));
		col2.setCellValueFactory(new PropertyValueFactory<UserVacancy, String>("jobTitle"));
		col3.setCellValueFactory(new PropertyValueFactory<UserVacancy, String>("description1"));
		col4.setCellValueFactory(new PropertyValueFactory<UserVacancy, String>("hiringManager"));
		col5.setCellValueFactory(new PropertyValueFactory<UserVacancy, String>("position"));
	

		buildData();
		
		FilteredList<UserVacancy> filteredData = new FilteredList<>(data, b -> true);
		vacancyname.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(UserVacancy -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (UserVacancy.getVacancyName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserVacancy.getJobTitle().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserVacancy.getDescription1().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserVacancy.getHiringManager().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserVacancy.getPosition().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else
					return false;

			});
		});
		
		SortedList<UserVacancy> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from addvacancy";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				UserVacancy user = new UserVacancy();
				user.vacancyName.set(resultSet.getString(1));
				user.jobTitle.set(resultSet.getString(2));
				user.description1.set(resultSet.getString(3));
				user.hiringManager.set(resultSet.getString(4));
				user.position.set(resultSet.getString(5));

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
