package search_vacancy;

import javafx.beans.property.SimpleStringProperty;

public class UserVacancy {
	public SimpleStringProperty vacancyName=new SimpleStringProperty();
	public SimpleStringProperty jobTitle=new SimpleStringProperty();
	public SimpleStringProperty description1=new SimpleStringProperty();
	public SimpleStringProperty hiringManager=new SimpleStringProperty();
	public SimpleStringProperty position=new SimpleStringProperty();

	public String getVacancyName(){
	       return vacancyName.get();
	   }
	
	public String getJobTitle(){
	       return jobTitle.get();
	   }
	
	public String getDescription1(){
	       return description1.get();
	   }
	
	public String getHiringManager(){
	       return hiringManager.get();
	   }
	
	public String getPosition(){
	       return position.get();
	   }

}
