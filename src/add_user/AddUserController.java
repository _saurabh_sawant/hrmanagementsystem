package add_user;

import java.net.URL;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import login_screen.LoginScreen;
import recruitment.Recruitment;

public class AddUserController implements Initializable {
	@FXML
	private ComboBox userrole;
	
	@FXML
	private ComboBox status;
	
	@FXML
	private TextField employeename;
	
	@FXML
	private TextField username;

	@FXML
	private TextField cpassword;

	@FXML
	private TextField password;
	
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button add;
	
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();

	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();

	}
	
	public void add(ActionEvent event) {
		System.out.println(userrole.getValue());
		System.out.println(status.getValue());
		System.out.println(employeename.getText());
		System.out.println(username.getText());
		System.out.println(password.getText());
		System.out.println(cpassword.getText());

		String query = "insert into admin1(userRole,status,employeeName,userName,password1,confirmPassword) values ('"
				+ userrole.getValue() + "', '" + status.getValue() + "','" + employeename.getText() + "','"
				+ username.getText() + "','" + password.getText() + "','" + cpassword.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());

	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list = FXCollections.observableArrayList("Admin", "ESS");
		userrole.setItems(list);
		//userRole.getItems().add(list);
		ObservableList<String> list2 = FXCollections.observableArrayList("Enabled", "Disabled");
		status.setItems(list2);
	}
}


