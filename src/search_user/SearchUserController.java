package search_user;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment.Recruitment;

public class SearchUserController implements Initializable {
	@FXML
	private Button search;
	
	@FXML
	private TextField username;
	
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private TableView<User> tableView;
	
	@FXML
	private TableColumn<User, String> col1;
	@FXML
	private TableColumn<User, String> col2;
	@FXML
	private TableColumn<User, String> col3;
	@FXML
	private TableColumn<User, String> col4;
	@FXML
	private TableColumn<User, String> col5;
	@FXML
	private TableColumn<User, String> col6;
	
	private ObservableList<User> data;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<User, String>("userRole"));
		col2.setCellValueFactory(new PropertyValueFactory<User, String>("employeeName"));
		col3.setCellValueFactory(new PropertyValueFactory<User, String>("status"));
		col4.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
		col5.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		col6.setCellValueFactory(new PropertyValueFactory<User, String>("confirmPassword"));

		buildData();

		FilteredList<User> filteredData = new FilteredList<>(data, b -> true);
		username.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(User -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (User.getUserRole().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getEmployeeName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getStatus().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getUserName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getPassword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getConfirmPassword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else
					return false;

			});
		});

		SortedList<User> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from admin1";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.userRole.set(resultSet.getString(2));
				user.employeeName.set(resultSet.getString(3));
				user.status.set(resultSet.getString(4));
				user.userName.set(resultSet.getString(5));
				user.password.set(resultSet.getString(6));
				user.confirmPassword.set(resultSet.getString(7));

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	public void search(ActionEvent event) {
		
	}
}
