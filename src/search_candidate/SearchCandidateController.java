package search_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import recruitment.Recruitment;


public class SearchCandidateController implements Initializable{
	@FXML
	private Button admin;
	
	@FXML
	private TextField candidatename;
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private TableView<UserCandidate> tableView;
	@FXML
	private TableColumn<UserCandidate, String> col1;
	@FXML
	private TableColumn<UserCandidate, String> col2;
	@FXML
	private TableColumn<UserCandidate, String> col3;
	@FXML
	private TableColumn<UserCandidate, String> col4;
	@FXML
	private TableColumn<UserCandidate, String> col5;
	@FXML
	private TableColumn<UserCandidate, String> col6;
	@FXML
	private TableColumn<UserCandidate, String> col7;
	@FXML
	private TableColumn<UserCandidate, String> col8;
	
	private ObservableList<UserCandidate> data;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		data = FXCollections.observableArrayList();

		col1.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("firstName"));
		col2.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("middleName"));
		col3.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("lastName"));
		col4.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("vacancy"));
		col5.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("keyword"));
		col6.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("email"));
		col7.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("contact"));
		col8.setCellValueFactory(new PropertyValueFactory<UserCandidate, String>("notes"));
		
		buildData();
		
		FilteredList<UserCandidate> filteredData = new FilteredList<>(data, b -> true);
		candidatename.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(UserCandidate -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (UserCandidate.getFirstName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getMiddleName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getLastName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getVacancy().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getKeyword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getEmail().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getContact().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (UserCandidate.getNotes().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else
					return false;

			});
		});
		
		SortedList<UserCandidate> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);
	}
	
	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select*from addcandidate";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				UserCandidate user = new UserCandidate();
				user.firstName.set(resultSet.getString(1));
				user.middleName.set(resultSet.getString(2));
				user.lastName.set(resultSet.getString(3));
				user.vacancy.set(resultSet.getString(4));
				user.keyword.set(resultSet.getString(5));
				user.email.set(resultSet.getString(6));
				user.contact.set(resultSet.getString(7));
				user.notes.set(resultSet.getString(8));

				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
