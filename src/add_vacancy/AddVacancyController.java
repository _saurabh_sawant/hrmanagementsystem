package add_vacancy;

import java.net.URL;
import java.util.ResourceBundle;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class AddVacancyController implements Initializable{
	@FXML
	private Button admin;	
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button add;
	
	@FXML
	private TextField vacancyname;
	
	@FXML
	private ComboBox jobtitle;
	
	@FXML
	private TextField description1;
	
	@FXML
	private TextField hiringmanager;
	
	@FXML
	private TextField position;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void add(ActionEvent event) {
		System.out.println(vacancyname.getText());
		System.out.println(jobtitle.getValue());
		System.out.println(description1.getText());
		System.out.println(hiringmanager.getText());
		System.out.println(position.getText());
		
		String query = "insert into addvacancy(vacancyname,jobtitle,description1,hiringmanager,position) values ('"
				+ vacancyname.getText() + "', '" + jobtitle.getValue() + "','" + description1.getText() + "','"
				+ hiringmanager.getText() + "','" + position.getText() + "');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur admin controller " + event.getEventType().getName());
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		ObservableList<String> list = FXCollections.observableArrayList("Account Assistant", "Content Specialist");
		jobtitle.setItems(list);
	}
}
