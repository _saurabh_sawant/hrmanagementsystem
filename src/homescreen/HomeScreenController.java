package homescreen;


import admin.Admin;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import recruitment.Recruitment;


public class HomeScreenController {
	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	public void admin(ActionEvent event) {
		new Admin().show();
		
	}
	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
}
