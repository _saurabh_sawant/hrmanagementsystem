package admin;

import recruitment.Recruitment;
import search_user.SearchUser;
import add_user.AddUser;
import delete_user.DeleteUser;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import login_screen.LoginScreen;

public class AdminController {
	@FXML
	private Button admin;

	@FXML
	private Button recruitment;

	@FXML
	private Button adduser;

	@FXML
	private Button searchuser;
	
	@FXML
	private Button dashboard;
	
	

	@FXML
	private Button deleteuser;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button exit;

	public void admin(ActionEvent event) {
		new Admin().show();

	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}

	public void adduser(ActionEvent event) {
		new AddUser().show();

	}

	public void searchuser(ActionEvent event) {
		new SearchUser().show();

	}

	public void deleteuser(ActionEvent event) {
		new DeleteUser().show();

	}
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();

	}
	public void cancel(ActionEvent event) {
		new HomeScreen().show();

	}
	public void exit(ActionEvent event) {
		new LoginScreen().show();

	}
}