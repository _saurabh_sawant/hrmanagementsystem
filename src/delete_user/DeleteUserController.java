package delete_user;

import admin.Admin;
import db_operations.DbUtil;
import homescreen.HomeScreen;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import recruitment.Recruitment;

public class DeleteUserController {
	@FXML
	private Button admin;
	
	@FXML
	private Button recruitment;
	
	@FXML
	private Button dashboard;
	
	@FXML
	private Button cancel;
	
	@FXML
	private Button delete;
	
	@FXML
	private TextField username;
	
	public void admin(ActionEvent event) {
		new Admin().show();
	}

	public void recruitment(ActionEvent event) {
		new Recruitment().show();
	}
	
	public void dashboard(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void cancel(ActionEvent event) {
		new HomeScreen().show();
	}
	
	public void delete(ActionEvent event) {
		System.out.println(username.getText());

		String query = " delete from admin1 where userName ='" + username.getText() + "';";
		System.out.println(query);
		DbUtil.executeQueryGetResult(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
	}
}
