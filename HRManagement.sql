CREATE DATABASE company;
use company;
create table addcandidate(
firstname varchar(50),
middlename varchar(50),
lastname varchar(50),
vacancy varchar(60),
email varchar(60),
contact varchar(60),
keyword varchar(50),
notes varchar(50)
);
create table admin1(
id int auto_increment,
userRole varchar(50),
employeeName varchar(60),
status varchar(60),
userName varchar(50),
password1 varchar(50),
confirmPassword varchar(50),
PRIMARY KEY (id)
);
create table user(
userName varchar(60),
password varchar(70)
);

create table addvacancy(
vacancyname varchar(70),
jobtitle varchar(50),
description1 varchar(80),
hiringmanager varchar(60),
position varchar(60)
);


drop table addcandidate;
drop table admin1;
drop table user;
select * from addcandidate;
drop table addvacancy; 
select*from user;
select*from admin1;
select * from addvacancy;